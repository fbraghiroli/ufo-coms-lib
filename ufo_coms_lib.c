#include <string.h>
#include "ufo_coms_lib.h"

int ufo_coms_frame_get(uint8_t *buf, uint8_t len, struct ufo_i2c_frame *frame)
{
	if (!buf || !frame)
		return -1;
	/* fetch and decode logical address */
        if (len < UFO_FRAME_LADDR_SIZE || len > (UFO_FRAME_SIZE))
                return -1;
        frame->pid = buf[UFO_FRAME_PID_OFFSET];
        frame->reg = buf[UFO_FRAME_REG_OFFSET];
	frame->payload = buf+UFO_FRAME_PLOAD_OFFSET;
	return 0;
}

int ufo_coms_frame_enc(uint8_t *buf, uint8_t len,
		       const struct ufo_i2c_frame *frame)
{
	if (!buf || !frame)
		return -1;
	if (len < UFO_FRAME_LADDR_SIZE || len > (UFO_FRAME_SIZE))
                return -1;

	buf[UFO_FRAME_PID_OFFSET] = frame->pid;
        buf[UFO_FRAME_REG_OFFSET] = frame->reg;
	memcpy(buf+UFO_FRAME_PLOAD_OFFSET, frame->payload,
	       len - UFO_FRAME_LADDR_SIZE);
	return 0;

}

int ufo_coms_pack_u16(void *buf, uint16_t v)
{
	if (!buf)
		return -1;
	memcpy(buf, &v, 2);
	return 0;
}

int ufo_coms_unpack_u16(const void *buf, uint16_t *v)
{
	if (!buf)
		return -1;
	memcpy(v, buf, 2);
	return 0;
}

int ufo_coms_pack_u32(void *buf, uint32_t v)
{
	if (!buf)
		return -1;
	memcpy(buf, &v, 4);
	return 0;
}

int ufo_coms_unpack_u32(const void *buf, uint32_t *v)
{
	if (!buf)
		return -1;
	memcpy(v, buf, 4);
	return 0;
}

int ufo_coms_pack_cfg(void *buf, const struct player_cfg *cfg)
{
	if (!buf || !cfg)
		return -1;
	uint8_t *u8_p = buf;
	u8_p = memcpy(u8_p, &cfg->en, 1) + 1;
	u8_p = memcpy(u8_p, &cfg->it, 1) + 1;
	u8_p = memcpy(u8_p, &cfg->selm, 1) + 1;
	u8_p = memcpy(u8_p, &cfg->playm, 1) + 1;
	u8_p = memcpy(u8_p, &cfg->fmin, 4) + 4;
	u8_p = memcpy(u8_p, &cfg->fmax, 4) + 4;
	return 0;
}

int ufo_coms_unpack_cfg(const void *buf, struct player_cfg *cfg)
{
	if (!buf || !cfg)
		return -1;
	const uint8_t *u8_p = buf;
	memcpy(&cfg->en, u8_p, 1);
	u8_p += 1;
	memcpy(&cfg->it, u8_p, 1);
	u8_p += 1;
	memcpy(&cfg->selm, u8_p, 1);
	u8_p += 1;
	memcpy(&cfg->playm, u8_p, 1);
	u8_p += 1;
	memcpy(&cfg->fmin, u8_p, 4);
	u8_p += 4;
	memcpy(&cfg->fmax, u8_p, 4);
	return 0;
}

int ufo_coms_pack_swtcfg(void *buf, const struct player_swtim_cfg *swt)
{
	if (!buf || !swt)
		return -1;
	uint8_t *u8_p = buf;
	u8_p = memcpy(u8_p, &swt->tim_freq, 4) + 4;
	u8_p = memcpy(u8_p, &swt->gpio_pulse, 4) + 4;
	u8_p = memcpy(u8_p, &swt->gpio_dir, 4) + 4;
	u8_p = memcpy(u8_p, &swt->gpio_sel, 4) + 4;
	u8_p = memcpy(u8_p, &swt->gpio_tk00, 4) + 4;
	u8_p = memcpy(u8_p, &swt->sel_en, 1) + 1;
	u8_p = memcpy(u8_p, &swt->tk00_en, 1) + 1;
	return 0;
}

int ufo_coms_unpack_swtcfg(const void *buf, struct player_swtim_cfg *swt)
{
	if (!buf || !swt)
		return -1;
	const uint8_t *u8_p = buf;
	memcpy(&swt->tim_freq, u8_p, 4);
	u8_p += 4;
	memcpy(&swt->gpio_pulse, u8_p, 4);
	u8_p += 4;
	memcpy(&swt->gpio_dir, u8_p, 4);
	u8_p += 4;
	memcpy(&swt->gpio_sel, u8_p, 4);
	u8_p += 4;
	memcpy(&swt->gpio_tk00, u8_p, 4);
	u8_p += 4;
	memcpy(&swt->sel_en, u8_p, 1);
	u8_p += 1;
	memcpy(&swt->tk00_en, u8_p, 1);
	return 0;
}
