
#ifndef UFO_PLAYER_SRC_UFO_COMS_LIB_H_
#define UFO_PLAYER_SRC_UFO_COMS_LIB_H_

#include "ufo_coms_defs.h"

int ufo_coms_frame_get(uint8_t *buf, uint8_t len, struct ufo_i2c_frame *frame);
int ufo_coms_frame_enc(uint8_t *buf, uint8_t len,
		       const struct ufo_i2c_frame *frame);

int ufo_coms_pack_u16(void *buf, uint16_t v);
int ufo_coms_unpack_u16(const void *buf, uint16_t *v);
int ufo_coms_pack_u32(void *buf, uint32_t v);
int ufo_coms_unpack_u32(const void *buf, uint32_t *v);
int ufo_coms_pack_cfg(void *buf, const struct player_cfg *cfg);
int ufo_coms_unpack_cfg(const void *buf, struct player_cfg *cfg);
int ufo_coms_pack_swtcfg(void *buf, const struct player_swtim_cfg *swt);
int ufo_coms_unpack_swtcfg(const void *buf, struct player_swtim_cfg *swt);

/* TODO: pack/unpack u8 u16 u32 */

#endif /* BOARDS_ARM_STM32_UFO_PLAYER_SRC_UFO_COMS_LIB_H_ */
