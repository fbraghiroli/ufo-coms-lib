
#ifndef BOARDS_ARM_STM32_UFO_PLAYER_SRC_UFO_COMS_DEFS_H_
#define BOARDS_ARM_STM32_UFO_PLAYER_SRC_UFO_COMS_DEFS_H_

#include <stdint.h>

#define UFO_PROTO_VER	1
#define UFO_DEVID	0x8907

#define UFO_FRAME_PLOAD_SIZE     32
#define UFO_FRAME_LADDR_SIZE     2
#define UFO_FRAME_SIZE           (UFO_FRAME_PLOAD_SIZE + UFO_FRAME_LADDR_SIZE)
#define UFO_FRAME_PID_OFFSET     0
#define UFO_FRAME_REG_OFFSET     1
#define UFO_FRAME_PLOAD_OFFSET   2
#define UFO_FRAME_SIZE_CALC(psize) (UFO_FRAME_LADDR_SIZE + (psize))

#define UFO_CBK_DEVID_SIZE	2
#define UFO_CBK_DEVUID_SIZE	12
#define UFO_CBK_PROTO_VER_SIZE	1
#define UFO_CBK_SYSTIME_SIZE	4
#define UFO_CBK_RTC_SIZE	4
#define UFO_CBK_CFG_SIZE	12
#define UFO_CBK_CFG_SWTIM_SIZE	22
#define UFO_CBK_PLAYF_SIZE	4
#define UFO_CBK_SIZE_MAX	UFO_CBK_CFG_SWTIM_SIZE

/* from stm32 gpio definitions */
#define UFO_GPIO_PORTA                  (0 << 4)
#define UFO_GPIO_PORTB                  (1 << 4)
#define UFO_GPIO_PORTC                  (2 << 4)
#define UFO_GPIO_PORTD                  (3 << 4)
#define UFO_GPIO_PORTE                  (4 << 4)
#define UFO_GPIO_PORTF                  (5 << 4)
#define UFO_GPIO_PORTG                  (6 << 4)
#define UFO_GPIO_PIN(x)                 ((x) << 0)


enum ufo_i2c_cbk_id {
	UFO_CBKID_DEVID,
	UFO_CBKID_DEVUID,
	UFO_CBKID_PROTO_VER,
	UFO_CBKID_SYSTIME,
	UFO_CBKID_RTC,
	UFO_CBKID_CFG,
	UFO_CBKID_CFG_SWTIM,
	UFO_CBKID_PLAYF,
	UFO_CBKID_TBBADD,
	UFO_CBKID_DOHOME,
	UFO_CBKID_TBBCLEAR,
	UFO_CBKIDCNT
};

enum player_iface_type {
	PLAYER_IT_SWTIM,
	PLAYER_IT_PWM,
};

enum player_sel_mode {
	PLAYER_SELM_NONE,
	PLAYER_SELM_FIX,
	PLAYER_SELM_OFF,
	PLAYER_SELM_ON,
	PLAYER_SELM_AUTO,
};

enum player_play_mode {
	PLAYER_PLAYM_DIRECT,
	PLAYER_PLAYM_BUFFERED,
};

struct player_swtim_cfg {
	uint32_t tim_freq;
	uint32_t gpio_pulse;
	uint32_t gpio_dir;
	uint32_t gpio_sel;
	uint32_t gpio_tk00;
	uint8_t sel_en;
	uint8_t tk00_en;
};

struct player_cfg {
	uint8_t en;
	enum player_iface_type it;
	enum player_sel_mode selm;
	enum player_play_mode playm;
	uint32_t fmin;	/* mHz */
	uint32_t fmax;	/* mHz */
};

enum player_msg_id {
	PLAYER_MID_HOME,
	PLAYER_MID_TBB_CLEAR
};

struct ufo_i2c_frame {
	uint8_t pid;
	uint8_t reg;
	uint8_t *payload;
};

#endif /* BOARDS_ARM_STM32_UFO_PLAYER_SRC_UFO_COMS_DEFS_H_ */
